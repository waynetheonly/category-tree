<?php

namespace App\Http\Controllers;

use App\Category;
use App\Classes\CategoryTree;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();

        foreach ($categories as $key => $category) {
            $parent = Category::find($category->parent_id);
            if ($parent) {
                $categories[$key]['parent_title'] = $parent->title;
            }
        }

        return view('categories.index', ['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();

        return view('categories.edit', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateCategoryFields($request);

        $category = new Category();

        $category->title = $request->title;
        $category->parent_id = isset($request->parent_id) ? $request->parent_id : 0;


        $category->save();

        return redirect()->route('categories.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $categories = Category::all();

        return view('categories.edit', ['category' => $category, 'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $this->validateCategoryFields($request);

        $category->update($request->all());

        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        Category::destroy($category->id);

        return redirect()->route('categories.index');
    }

    protected function validateCategoryFields($request)
    {
        $this->validate(
            $request,
            [
                'title' => 'required|max:255',
            ]
        );
    }

    public function tree($type = null)
    {
        $tree = new CategoryTree();
        $empty = false;

        switch ($type) {
            case 'recursive':
                $tree->generateRecursive();
                break;
            case 'iterative' :
                $tree->generateIterative();

                break;
            default:
                $empty = true;
                break;
        }

        return view(
            'categories.tree',
            array(
                'type' => $type,
                'list' => $empty ?: $tree->get(),
                'empty' => $empty
            )
        );
    }
}
