<?php

use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = json_decode(file_get_contents(dirname(__FILE__) . '/data/categories.json'), true);
        $timestamp = Carbon::now()->toDateTimeString();

        foreach ($categories as $category) {
            DB::table('categories')->insert([
                'title' => $category['title'],
                'parent_id' => $category['parent_id'],
                'created_at' => $timestamp,
                'updated_at' => $timestamp,
            ]);
        }
    }
}
