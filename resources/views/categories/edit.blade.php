@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-4 mx-auto">
            <h1>{{ isset($category) ? 'Edit' : 'Add' }} category</h1>
            @if (count($errors))
                <div class="alert alert-danger">
                    <ul class="my-0">
                        @foreach ($errors->all() as $error)
                            <li>
                                {{ $error }}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form role="form" method="POST"
                  action="{{ isset($category) ? route('categories.update', $category->id) : route('categories.store') }}">
                @if(isset($category))
                    {{ method_field('PATCH') }}
                @endif
                {{ csrf_field() }}
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-sm-8">
                                <label for="title">Title</label>
                                <input id="title" class="form-control" name="title" type="text"
                                       value="{{ old('title') ? old('title') : (isset($category->title) ? $category->title : null) }}">
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="category">Parent</label>
                                <select id="category" class="form-control"
                                        name="parent_id"{{ count($categories) < 1 ? 'disabled' : ''}}>
                                    <option value="0">Select</option>
                                    @if(isset($categories) && count($categories) > 0)
                                        @foreach($categories as $parent)
                                            @if((isset($category->id) && $category->id == $parent->id))
                                            @else
                                                <option value="{{ $parent->id }}"
                                                        @if((isset($category) && ($category->parent_id == $parent->id))) selected @endif>
                                                    {{ $parent->title }}
                                                </option>
                                            @endif
                                        @endforeach
                                    @else
                                        <option disabled>No categories</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="btn-block">
                            <a href="{{ route('categories.index') }}" class="btn btn-secondary float-left">Cancel</a>
                            <button type="submit" class="btn btn-primary float-right"><i
                                        class="glyphicon glyphicon-floppy-disk"></i> Save
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection