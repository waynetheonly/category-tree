@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-8">
                        <h1>Categories</h1>
                    </div>
                    <div class="col-sm-4">
                        <a href="{{ route('categories.create') }}" class="btn btn-primary float-right">Create new</a>
                    </div>
                </div>
                @if(isset($categories) && count($categories)>0)
                    <div class="card border-0">
                        <div class="card-body p-0">
                            <table class="table table-bordered mb-0">
                                <thead>
                                <tr class="d-flex">
                                    <th scope="col" class="col text-center">ID</th>
                                    <th scope="col" class="col-5">Title</th>
                                    <th scope="col" class="col-4">Parent</th>
                                    <th scope="col" class="col">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($categories as $category)
                                    <tr class="d-flex">
                                        <th scope="row" class="col text-center py-1">
                                            <span class="align-middle">{{ $category->id }}</span>
                                        </th>
                                        <td class="col-5 py-1">
                                            <span class="align-middle">{{ $category->title }}</span>
                                        </td>
                                        <td class="col-4 py-1">
                                            <span class="align-middle">{{ $category->parent_title }}</span>
                                        </td>
                                        <td class="col py-1 text-center">
                                            <a class="btn btn-outline-success btn-sm"
                                               href="{{ route('categories.edit', $category)}}">Edit</a>
                                            <button class="btn btn-outline-danger btn-sm"
                                                    data-toggle="modal"
                                                    data-target="#modalConfirmDelete"
                                                    data-title="{{ $category->title }}"
                                                    data-url="{{ route('categories.destroy', $category)}}">Delete
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                @else
                    <div class="card">
                        <div class="card-body">
                            <p class="text-center m-0">No categories yet.</p>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
