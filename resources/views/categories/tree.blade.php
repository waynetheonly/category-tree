@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                @if(isset($empty) && $empty)
                    <div class="alert alert-warning" role="alert">
                        You passed the wrong generation method to the URL. Try
                        <a href="{{ route('tree', ['type' => 'recursive']) }}">/recursive</a> or
                        <a href="{{ route('tree', ['type' => 'iterative']) }}">/iterative</a>
                        for results.
                    </div>
                @else
                    <h1>{{ !isset($type) ?: ucfirst($type) }} tree</h1>
                    <div class="card">
                        <div class="card-body">
                            @if(empty($list))
                                <div class="text-center">
                                    No categories found
                                </div>
                            @else
                                {!! $list !!}
                            @endif
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
